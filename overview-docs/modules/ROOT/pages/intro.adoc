= What is TomEE?

{url-project}[Apache TomEE], pronounced "`Tommy`", is an all-Apache Java EE Web Profile certified stack where Apache Tomcat is top dog.

Apache TomEE is assembled from a vanilla Apache Tomcat zip file.
We start with Apache Tomcat, add our jars and zip up the rest.
The result is Tomcat with added EE features, which we call TomEE.

== TomEE Flavors

Apache TomEE comes with four different flavors: Web Profile, JAX-RS, Plus, and Plume.

Apache TomEE Web Profile:: delivers Servlets, JSP, JSF, JTA, JPA, CDI, Bean Validation and EJB Lite.
Apache TomEE JAX-RS (RESTfull Services):: delivers the Web Profile plus JAX-RS (RESTfull Services).
Apache TomEE Plus:: delivers all that is in the Web Profile and JAX-RS (RESTfull Services), plus EJB Full, Java EE Connector Architecture, JMS (Java Message Service) and JAX-WS (Web Services).
Apache TomEE Plume:: delivers all that is in the Plus Profile, but includes Mojarra and EclipseLink support.

All versions of TomEE can be downloaded from the {url-project}/download-ng.html[TomEE project site].

== Get Support

You can get support for TomEE from multiple vendors.
See xref:support.adoc[TomEE Support] for details.

== Learn More

Now that you know a little bit about TomEE, it's time to put it to work.
Where you go next depend on whether you're interested in administration or application development.

* xref:admin:configuration:index.adoc[Administration]
* xref:developer::index.adoc[Application Development]
